#!/usr/bin/env python3

import grpc

import simple_pb2_grpc
import simple_pb2


if __name__ == '__main__':
    channel = grpc.insecure_channel('localhost:5757')
    stub = simple_pb2_grpc.SampleServiceStub(channel)
    
    request = simple_pb2.InfoRequest(printMe="Hello World!")
    print(stub.getSystemInfo(request))
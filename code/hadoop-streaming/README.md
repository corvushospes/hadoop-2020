yarn jar /usr/lib/hadoop-mapreduce/hadoop-streaming.jar\
 -input weather.csv\
 -output stat\
 -file mapper.py\
 -file reducer.py\
 -mapper "python mapper.py"\
 -reducer "python reducer.py"